import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FullCalendarModule } from '@fullcalendar/angular'; // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import interactionPlugin from '@fullcalendar/interaction'; // a plugin!
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AgendaComponent } from './agenda/agenda.component';
import { HeaderComponent } from './header/header.component';
import { SidebarsComponent } from './sidebars/sidebars.component';
import { PaginaPrincipalComponent } from './pagina-principal/pagina-principal.component';
import { RegistroPacienteComponent } from './registro-paciente/registro-paciente.component';
import { CitasPacienteComponent } from './citas-paciente/citas-paciente.component';
import { AgendaMedicoComponent } from './agenda-medico/agenda-medico.component';
import { PaginaPrincipalPacienteComponent } from './pagina-principal-paciente/pagina-principal-paciente.component';
import { PaginaPrincipalMedicoComponent } from './pagina-principal-medico/pagina-principal-medico.component';
import { HeaderPacienteComponent } from './header-paciente/header-paciente.component';
import { ObservacionesComponent } from './observaciones/observaciones.component';
import { VisionComponent } from './vision/vision.component';
import { MisionComponent } from './mision/mision.component';




FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  timeGridPlugin,
  listPlugin,
  interactionPlugin
]);


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AgendaComponent,
    HeaderComponent,
    SidebarsComponent,
    PaginaPrincipalComponent,
    RegistroPacienteComponent,
    CitasPacienteComponent,
    AgendaMedicoComponent,
    PaginaPrincipalPacienteComponent,
    PaginaPrincipalMedicoComponent,
    HeaderPacienteComponent,
    ObservacionesComponent,
    VisionComponent,
    MisionComponent
  ],
  imports: [
    
    BrowserModule,
    AppRoutingModule,
    FullCalendarModule // register FullCalendar with you app  
     
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
