import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AgendaMedicoComponent } from './agenda-medico/agenda-medico.component';
import { AgendaComponent } from './agenda/agenda.component';
import { CitasPacienteComponent } from './citas-paciente/citas-paciente.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { MisionComponent } from './mision/mision.component';
import { ObservacionesComponent } from './observaciones/observaciones.component';
import { PaginaPrincipalMedicoComponent } from './pagina-principal-medico/pagina-principal-medico.component';
import { PaginaPrincipalPacienteComponent } from './pagina-principal-paciente/pagina-principal-paciente.component';
import { PaginaPrincipalComponent } from './pagina-principal/pagina-principal.component';
import { RegistroPacienteComponent } from './registro-paciente/registro-paciente.component';
import { SidebarsComponent } from './sidebars/sidebars.component';
import { VisionComponent } from './vision/vision.component';

const routes: Routes = [

  {path: '',component:PaginaPrincipalComponent},
  //{path:'**',redirectTo:'/'},
  {path: 'agenda',component:AgendaComponent},
  {path: 'login',component:LoginComponent}, 
  {path: 'paginaprincipal', component:PaginaPrincipalComponent},
  {path: 'agenda-medico',component:AgendaMedicoComponent},
  {path: 'registro-paciente',component:RegistroPacienteComponent},
  {path: 'citas-paciente',component:CitasPacienteComponent},
  {path: 'pagina-principal-paciente',component:PaginaPrincipalPacienteComponent},
  {path: 'pagina-principal-medico',component:PaginaPrincipalMedicoComponent},
  {path: 'observaciones',component:ObservacionesComponent},
  {path: 'vision',component: VisionComponent},
  {path: 'mision',component: MisionComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
