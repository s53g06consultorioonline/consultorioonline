import { TestBed } from '@angular/core/testing';

import { EventUtilsService } from './event-utils.service';

describe('EventUtilsService', () => {
  let service: EventUtilsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventUtilsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
