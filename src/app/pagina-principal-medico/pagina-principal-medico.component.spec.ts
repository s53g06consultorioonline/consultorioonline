import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaPrincipalMedicoComponent } from './pagina-principal-medico.component';

describe('PaginaPrincipalMedicoComponent', () => {
  let component: PaginaPrincipalMedicoComponent;
  let fixture: ComponentFixture<PaginaPrincipalMedicoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginaPrincipalMedicoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaPrincipalMedicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
